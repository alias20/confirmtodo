#import <substrate.h>
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <dlfcn.h>

@interface DialerController : UIViewController
@end

@interface MPRecentsTableViewController : UIViewController
@end

@interface MPFavoritesTableViewController : UIViewController
@end

@interface PHFrecentResultViewController : UIViewController
@end

@interface PHFaceTimePeoplePickerViewController : UIViewController
@end

@interface CNContactContentViewController : UIViewController
@end

@interface CNContactInlineActionsViewController : UIViewController
@end

@interface MessageViewController : UIViewController
@end

@interface ConversationViewController : UIViewController
@end

@interface CKChatController : UIViewController
@end

@interface MFMailComposeController : UIViewController
@end

@interface WAChatViewController : UIViewController
@end

@interface VIBConversationDetailVC : UIViewController
@end

@interface CNActionView : UIView
@end

@interface WAChatBar : UIView
@end

@interface CKMessageEntryView : UIView
@end

@interface EmoticonBoardCrossCollectionQQEmojiPageCell : UIView 
@end

@interface FTRecentsListViewController : UIViewController
@end

@interface UserMessageCellView : UIView 
@end

@interface ConfirmManager: NSObject
+ (instancetype)sharedInstance;
@end

@interface _TtC6TalkUI13ClosureSleeve : NSObject
-(void)confirmInvoke;
-(void)invoke;
@end

static BOOL kEnabled = false;
static BOOL kUseLine = false;
static BOOL kUseSignal = false;
static BOOL kUseReddit = false;
static BOOL kUseSMS = false;
static BOOL kUseMail = false;
static BOOL kUsePhone = false;
static BOOL kUseWhatsAppMsg = false;
static BOOL kUseWhatsAppCall = false;
static BOOL kUseWhatsAppVideoCall = false;
static BOOL kUseViber = false;
static BOOL kUseWeChat = false;
static BOOL kUseFaceTime = false;
static BOOL kUseKaTalkMsg = false;
static BOOL kUseKaTalkCall = false;

static void loadPrefs(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo) {
	NSDictionary *prefs = [NSDictionary dictionaryWithContentsOfFile:@"/var/jb/var/mobile/Library/Preferences/alias20.confirmtodo.plist"];
	if([prefs objectForKey:@"enabled"]) kEnabled = [[prefs objectForKey:@"enabled"] boolValue]; 
	else kEnabled = false;
	if([prefs objectForKey:@"useLine"]) kUseLine = [[prefs objectForKey:@"useLine"] boolValue];
	else kUseLine = false;
	if([prefs objectForKey:@"useSignal"]) kUseSignal = [[prefs objectForKey:@"useSignal"] boolValue];
	else kUseSignal = false;
	if([prefs objectForKey:@"useReddit"]) kUseReddit = [[prefs objectForKey:@"useReddit"] boolValue];
	else kUseReddit = false;
	if([prefs objectForKey:@"useSMS"]) kUseSMS = [[prefs objectForKey:@"useSMS"] boolValue];
	else kUseSMS = false;
	if([prefs objectForKey:@"useMail"]) kUseMail = [[prefs objectForKey:@"useMail"] boolValue];
	else kUseMail = false;
	if([prefs objectForKey:@"usePhone"]) kUsePhone = [[prefs objectForKey:@"usePhone"] boolValue];
	else kUsePhone = false;
	if([prefs objectForKey:@"useWhatsAppMsg"]) kUseWhatsAppMsg = [[prefs objectForKey:@"useWhatsAppMsg"] boolValue];
	else kUseWhatsAppMsg = false;
	if([prefs objectForKey:@"useWhatsAppCall"]) kUseWhatsAppCall = [[prefs objectForKey:@"useWhatsAppCall"] boolValue];
	else kUseWhatsAppCall = false;
	if([prefs objectForKey:@"useWhatsAppVideoCall"]) kUseWhatsAppVideoCall = [[prefs objectForKey:@"useWhatsAppVideoCall"] boolValue];
	else kUseWhatsAppVideoCall = false;
	if([prefs objectForKey:@"useViber"]) kUseViber = [[prefs objectForKey:@"useViber"] boolValue];
	else kUseViber = false;
	if([prefs objectForKey:@"useWeChat"]) kUseWeChat = [[prefs objectForKey:@"useWeChat"] boolValue];
	else kUseWeChat = false;
	if([prefs objectForKey:@"useFaceTime"]) kUseFaceTime = [[prefs objectForKey:@"useFaceTime"] boolValue];
	else kUseFaceTime = false;

	if([prefs objectForKey:@"useKaTalkMsg"]) kUseKaTalkMsg = [[prefs objectForKey:@"useKaTalkMsg"] boolValue];
	else kUseKaTalkMsg = false;
	if([prefs objectForKey:@"useKaTalkCall"]) kUseKaTalkCall = [[prefs objectForKey:@"useKaTalkCall"] boolValue];
	else kUseKaTalkCall = false;
	
}


@implementation ConfirmManager
+ (instancetype)sharedInstance {
    static ConfirmManager* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ConfirmManager alloc] init];
    });

    return sharedInstance;
}

- (void)showConfirmationAlertWithCompletion:(void (^)(void))completionBlock viewController:(UIViewController *)arg1 title:(NSString*)title message:(NSString*)msg  {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *callAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        if (completionBlock) {
            completionBlock();
        }
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:callAction];
    [arg1 presentViewController:alertController animated:YES completion:nil];
}

-(UIViewController *)findViewController:(UIView *)view {
	UIViewController *viewController = nil;
	UIResponder *nextResponder = view.nextResponder;
	while (nextResponder) {
    	if ([nextResponder isKindOfClass:[UIViewController class]]) {
        	viewController = (UIViewController *)nextResponder;
        	break;
    	}
    	nextResponder = nextResponder.nextResponder;
	}
	return viewController;
}

-(UITableViewCell *)findUserMessageCell:(UIView *)view {
	UITableViewCell *userMessageCell = nil;
	UIResponder *nextResponder = view.nextResponder;
	while (nextResponder) {
    	if ([nextResponder isKindOfClass:NSClassFromString(@"KakaoTalk.UserMessageCell")]) {
        	userMessageCell = (UITableViewCell *)nextResponder;
        	break;
    	}
    	nextResponder = nextResponder.nextResponder;
	}
	return userMessageCell;
}
@end

//FaceTime
%group FaceTimeHook
%hook FTRecentsListViewController
-(void)collectionView:(id)arg1 didSelectItemAtIndexPath:(id)arg2 {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1, arg2);} viewController: self title: @"Face Time Alert" message: @"Are you Sure To Face Video Call?"];
}
%end

%hook PHFaceTimePeoplePickerViewController
-(void)startAudioCallButtonTapped {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: self title: @"Face Time Alert" message: @"Are you Sure To Face Audio Call?"];
}
-(void)startVideoCallButtonTapped {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: self title: @"Face Time Alert" message: @"Are you Sure To Face Video Call?"];
}
%end

%hook FaceTime_PeoplePickerActionBar
-(void)videoButtonTapped:(id)arg1 {
	UIViewController *viewCtl = [[ConfirmManager sharedInstance] findViewController: self];
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: viewCtl title: @"Face Time Alert" message: @"Are you Sure To Face Video Call?"];
}

-(void)audioButtonTapped:(id)arg1 {
	UIViewController *viewCtl = [[ConfirmManager sharedInstance] findViewController: self];
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: viewCtl title: @"Face Time Alert" message: @"Are you Sure To Face Audio Call?"];
}
%end

%hook PHFrecentResultViewController 
-(void)performDial:(id)arg1 withService:(int)arg2 {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1, arg2);} viewController: self title: @"Face Time Alert" message: @"Are you Sure To Face Call?"];
}

- (void)performDialRequest:(id)arg1 {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: self title: @"Face Time Alert" message: @"Are you Sure To Face Call?"];
}
%end
%end

%group LineHook
%hook MessageViewController
-(void)sendButtonPressed {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: self title: @"Line Alert" message: @"Are you Sure To Send?"];
}
%end

%hook LineMessagingUI_ChatInputBar
-(void)buttonPressed:(id)arg1 {
	UIViewController *viewCtl = [[ConfirmManager sharedInstance] findViewController: self];
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: viewCtl title: @"Line Alert" message: @"Are you Sure To Send?"];
}
%end
%end

%group SignalHook
%hook ConversationViewController
-(void)sendButtonPressed {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: self title: @"Signal Message Alert" message: @"Are you sure to send it?"];
}
%end

%hook Signal_ConversationInputToolbar
-(void)sendButtonPressed {
	UIViewController *viewCtl = [[ConfirmManager sharedInstance] findViewController: self];
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: viewCtl title: @"Signal Message Alert" message: @"Are you sure to send it?"];
}
%end
%end

%group RedditHook
%hook Chat_RedditChat_ChatTextInputViewController
-(void)didTapSendButton:(id)arg1 {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: self title: @"Reddit Message Alert" message: @"Are you sure to send it?"];
}
%end

%hook Reddit_MessageComposeViewController
-(void)didTapSendButtonWithSendButton:(id)arg1 {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: self title: @"Reddit Message Alert" message: @"Are you sure to send it?"];
}
%end
%end

%group SMSHook
// %hook CKChatController
// -(void)messageEntryViewSendButtonHit:(id)arg1 {
// 	// NSLog(@"[ConfirmToDo] SMSHook trace: %@", [NSThread callStackSymbols]);
// 	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: self title: @"SMS Alert" message: @"Are you Sure To Send?"];
// }

// -(void)sendComposition:(id)arg1 {
// 	NSLog(@"[ConfirmToDo] SMSHook trace: %@", [NSThread callStackSymbols]);
// 	// [[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: self title: @"SMS Alert" message: @"Are you Sure To Send?"];
// }


// %end

// %hook CKNotificationChatController
// -(void)sendComposition:(id)arg1 {
// 	NSLog(@"[ConfirmToDo] SMSHook trace: %@", [NSThread callStackSymbols]);
// 	// [[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: self title: @"SMS Alert" message: @"Are you Sure To Send?"];
// }
// %end

// %hook CKActionMenuGestureRecognizerButton
// -(void)gestureRecognized:(id)arg1 {
// 	NSLog(@"[ConfirmToDo] gestureRecognized %@", arg1);
// 	%orig;
// }
// %end

%hook CKMessageEntryView
-(void)touchUpInsideSendButton:(id)arg1 {
	UIViewController *viewCtl = [[ConfirmManager sharedInstance] findViewController: self];
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: viewCtl title: @"SMS Alert" message: @"Are you Sure To Send?"];
}
%end
%end

%group MailHook
%hook MFMailComposeController
-(void)send:(id)arg1 {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: self title: @"Mail Alert" message: @"Are you Sure To Send?"];
}
%end
%end

%group PhoneHook
%hook DialerController
-(void)_callButtonPressed:(id)arg1 {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: self title: @"Phone Alert" message: @"Are you Sure To Call?"];
}
%end

%hook MPRecentsTableViewController
-(void)tableView:(id)arg1 didSelectRowAtIndexPath:(id)arg2 {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1, arg2);} viewController: self title: @"Phone Recents Alert" message: @"Are you Sure To Tap?"];
}
%end

%hook MPFavoritesTableViewController
-(void)tableView:(id)arg1 didSelectRowAtIndexPath:(id)arg2 {
	UITableViewCell *cell = [arg1 cellForRowAtIndexPath:arg2];
	NSString *actionType = MSHookIvar<NSString *>(cell, "_actionType");
	if([actionType isEqualToString: @"VideoCallActionType"]) {
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1, arg2);} viewController: self title: @"Phone Favorites Alert" message: @"Are you Sure To Video Call?"];
	}
	else if([actionType isEqualToString: @"AudioCallActionType"]) {
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1, arg2);} viewController: self title: @"Phone Favorites Alert" message: @"Are you Sure To Audio Call?"];
	}
	else if([actionType isEqualToString: @"MailActionType"]) {
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1, arg2);} viewController: self title: @"Phone Favorites Alert" message: @"Are you Sure To Send Mail?"];
	}
	else if([actionType isEqualToString: @"MessageActionType"]) {
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1, arg2);} viewController: self title: @"Phone Favorites Alert" message: @"Are you Sure To Send Message?"];
	} 
	else {
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1, arg2);} viewController: self title: @"Phone Favorites Alert" message: @"Are you Sure To Tap?"];
	}
}
%end
%end

%group ContactsHook
%hook CNContactContentViewController
- (void)tableView:(id)arg1 didSelectRowAtIndexPath:(id)arg2 {
	UITableViewCell *cell = [arg1 cellForRowAtIndexPath:arg2];
	// NSLog(@"[ConfirmToDo] CNContactContentViewController cell; %@", cell);
	if ([cell isKindOfClass:NSClassFromString(@"CNPropertyEmailAddressCell")]) {
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1, arg2);} viewController: self title: @"Contact Alert" message: @"Are you Sure To Send Mail?"];
	}
	else if ([cell isKindOfClass:NSClassFromString(@"CNFaceTimeCell")]) {
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1, arg2);} viewController: self title: @"Contact Alert" message: @"Are you Sure To Video Call?"];
	} 
	else if ([cell isKindOfClass:NSClassFromString(@"CNPropertyPhoneNumberCell")]) {
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1, arg2);} viewController: self title: @"Contact Alert" message: @"Are you Sure To Call?"];
	} 
	// else if ([cell isKindOfClass:NSClassFromString(@"CNPropertyEmailAddressCell")]) {

	// }
	// else if ([cell isKindOfClass:NSClassFromString(@"CNPropertyEmailAddressCell")]) {

	// }
	else {
		%orig;
		//[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1, arg2);} viewController: self title: @"Contact Alert" message: @"Are you Sure To Tap?"];
	}
}

// - (void)action:(id)arg1 presentViewController:(id)arg2 sender:(id)arg3 {
// 	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1, arg2, arg3);} viewController: self title: @"Contact Alert" message: nil];
// }
%end

// %hook CNContactInlineActionsViewController
// - (void)didSelectAction:(id)arg1 withSourceView:(id)arg2 longPress:(bool)arg3 {
// 	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1, arg2, arg3);} viewController: self title: @"Contact Alert" message: @"Are you Sure To Tap?"];
// }
// %end

%hook CNActionView
-(void)handleTapGesture {
	UIViewController *viewCtl = [[ConfirmManager sharedInstance] findViewController: self];
	NSString *actionType = MSHookIvar<NSString *>(self, "_type");
	if([actionType isEqualToString: @"VideoCallActionType"]) {
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: viewCtl title: @"Contact Alert" message: @"Are you Sure To Video Call?"];
	}
	else if([actionType isEqualToString: @"AudioCallActionType"]) {
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: viewCtl title: @"Contact Alert" message: @"Are you Sure To Audio Call?"];
	}
	else if([actionType isEqualToString: @"MailActionType"]) {
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: viewCtl title: @"Contact Alert" message: @"Are you Sure To Send Mail?"];
	}
	else if([actionType isEqualToString: @"MessageActionType"]) {
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: viewCtl title: @"Contact Alert" message: @"Are you Sure To Send Message?"];
	} 
	else {
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: viewCtl title: @"Contact Alert" message: @"Are you Sure To Tap?"];
	}
}
%end
%end

%group WhatsAppHook
%hook WAChatViewController
-(void)callButtonTapped:(id)arg1 {
	if(kUseWhatsAppCall)
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: self title: @"WhatsApp Alert" message: @"Are you Sure To Call?"];
	else
		%orig(arg1);
}

-(void)videoCallButtonTapped:(id)arg1 {
	if(kUseWhatsAppVideoCall)
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: self title: @"WhatsApp Alert" message: @"Are you Sure To Video Call?"];
	else
		%orig(arg1);
}
%end

%hook WAChatBar
-(void)sendButtonTapped:(id)arg1 {
	if(kUseWhatsAppMsg) {
		UIViewController *viewCtl = [[ConfirmManager sharedInstance] findViewController: self];
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: viewCtl title: @"WhatsApp Alert" message: @"Are you Sure To Send?"];
	}
	else {
		%orig(arg1);
	}
}
%end
%end

%group ViberHooks
%hook VIBConversationDetailVC
-(void)messageInputFieldDidPressSendButton:(id)arg1 {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig(arg1);} viewController: self title: @"Viber Alert" message: @"Are you Sure To Send?"];
}
%end
%end

%group WeChatHooks
%hook EmoticonBoardCrossCollectionQQEmojiPageCell
-(void)onTapSendButton {
	UIViewController *viewCtl = [[ConfirmManager sharedInstance] findViewController: self];
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: viewCtl title: @"WeChat Alert" message: @"Are you Sure To Send?"];
}
%end
%end

%group KakaoTalkHooks
// %hook KakaoTalk_ProfileBottomButton
// -(void)layoutSubview {
	// for (UIGestureRecognizer *gestureRecognizer in button.gestureRecognizers) {
    // 	if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
    //     	UITapGestureRecognizer *tapGestureRecognizer = (UITapGestureRecognizer *)gestureRecognizer;

	// 		break;
    // 	}
	// }
// 	%orig;
// }
// %end

%hook KakaoTalk_MVoipMessageCellView
-(void)didActionButtonClicked {
	if(!kUseKaTalkCall) {
		%orig;
		return;
	}

	UIViewController *viewCtl = [[ConfirmManager sharedInstance] findViewController: self];
	UITableViewCell *userMessageCell = [[ConfirmManager sharedInstance] findUserMessageCell: self];
	NSString *messageDescription = userMessageCell.accessibilityLabel;
	if([messageDescription containsString:@"보이스톡"] || [messageDescription containsString:@"Voice Call"])
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: viewCtl title: @"KakaoTalk Alert" message: @"Are you Sure To Voice Talk?"];
	else if([messageDescription containsString:@"페이스톡"] || [messageDescription containsString:@"Video Call"])
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: viewCtl title: @"KakaoTalk Alert" message: @"Are you Sure To Face Talk?"];
	else
		[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: viewCtl title: @"KakaoTalk Alert" message: @"Are you Sure To Voice / Face Talk?"];
}
%end

%hook KakaoTalk_ChatInputBarView
-(void)onTouchUpSendButton {
	if(!kUseKaTalkMsg) {
		%orig;
		return;
	}
	UIViewController *viewCtl = [[ConfirmManager sharedInstance] findViewController: self];
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: viewCtl title: @"KakaoTalk Alert" message: @"Are you Sure To Send?"];
}
%end

// %hook KakaoTalk_ChattingViewController
// -(void)keyboardMenuDidSelectVoiceTalk {
// 	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: self title: @"KakaoTalk Alert" message: @"Are you Sure To Voice Talk?"];
// }

// -(void)keyboardMenuDidSelectFreeCall {
// 	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{%orig;} viewController: self title: @"KakaoTalk Alert" message: @"Are you Sure To Call?"];
// }
// %end

// %hook UIAlertController
// +(id)alertControllerWithTitle:(id)arg1 message:(id)arg2 preferredStyle:(long long)arg3 {
	// if([arg2 hasPrefix:@"Data charges"]) {
	// 	NSArray *tr = [NSThread callStackSymbols];
	// 	for(NSString *tr0 in tr)
	// 		NSLog(@"[ConfirmToDo] tr0: %@", tr0);
	// }
// 	return %orig;
// }
// %end

static UIViewController *profileViewController = nil;
%hook KakaoTalk_ProfileBottomButton
-(void)layoutSubviews {
	%orig;
	if(!kUseKaTalkCall) return;	
	
	UIView *ourView = (UIView *)self;
	if([ourView.accessibilityLabel isEqualToString:@"Voice Call"] || [ourView.accessibilityLabel isEqualToString:@"Video Call"] || [ourView.accessibilityLabel isEqualToString:@"보이스톡"] || [ourView.accessibilityLabel isEqualToString:@"페이스톡"]) {
		for (UIGestureRecognizer *gestureRecognizer in ourView.gestureRecognizers) {
        	if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
				// NSLog(@"[ConfirmToDo] gestureRecognizer: %@", gestureRecognizer);

				profileViewController = [[ConfirmManager sharedInstance] findViewController: self];

				//Action 
				NSMutableArray *targets = MSHookIvar<NSMutableArray *>(gestureRecognizer, "_targets");
				// id target = MSHookIvar<id>(targets[0], "_target");
				// SEL action = MSHookIvar<SEL>(targets[0], "_action");

				if([ourView.accessibilityLabel isEqualToString:@"Voice Call"] || [ourView.accessibilityLabel isEqualToString:@"보이스톡"])
					MSHookIvar<SEL>(targets[0], "_action") = @selector(confirmVoiceCall);
				else if([ourView.accessibilityLabel isEqualToString:@"Video Call"] || [ourView.accessibilityLabel isEqualToString:@"페이스톡"])
					MSHookIvar<SEL>(targets[0], "_action") = @selector(confirmVideoCall);

				// IMP invoke = [target methodForSelector:@selector(invoke)];
				// [target performSelector:@selector(invoke)];

				//Disable function
				// [gestureRecognizer removeTarget:target action:@selector(invoke)];

				// NSLog(@"[ConfirmToDo] gestureRecognizer target: %@, invoke IMP:%p, profileViewController: %@", target, invoke, profileViewController);
				break;
        	}
    	}
	}
	// %orig;
}
%end

%hook _TtC6TalkUI13ClosureSleeve
%new 
-(void)confirmVoiceCall {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{[self invoke];} viewController: profileViewController title: @"KakaoTalk Alert" message: @"Are you Sure To Voice Call?"];
}

%new
-(void)confirmVideoCall {
	[[ConfirmManager sharedInstance] showConfirmationAlertWithCompletion:^{[self invoke];} viewController: profileViewController title: @"KakaoTalk Alert" message: @"Are you Sure To Face Call?"];
}
%end
%end

// void (*old_dispatch_async)(dispatch_queue_t dq, void (^work)(void));
// void new_dispatch_async(dispatch_queue_t dq, void (^work)(void)) {
// //   NSLog(@"[ConfirmToDo] new_dispatch_async: %@", [NSThread callStackSymbols]);
//   NSArray *tr = [NSThread callStackSymbols];
// 		for(NSString *tr0 in tr)
// 			NSLog(@"[ConfirmToDo] new_dispatch_async tr0: %@", tr0);
//   return
//   old_dispatch_async(dq, work);
// }

// static void (*orig_addTapHandler)(id arg1, id arg2);
// static void hook_addTapHandler(id arg1, id arg2) {
// 	// NSLog(@"[ConfirmToDo] hook_addTapHandler arg1: %@, arg2: %@", arg1, arg2);
// 	orig_addTapHandler(arg1, arg2);
// }


%ctor {
	@autoreleasepool {
		CFNotificationCenterRef center = CFNotificationCenterGetDarwinNotifyCenter();
		CFNotificationCenterAddObserver(center, NULL, loadPrefs, CFSTR("alias20.confirmtodo.prefschanged"), NULL, CFNotificationSuspensionBehaviorCoalesce);
		loadPrefs(0, 0, 0, 0, 0);

		if(!kEnabled)
			return;

		if(kUseFaceTime)
			%init(FaceTimeHook, FaceTime_PeoplePickerActionBar = NSClassFromString(@"FaceTime.PeoplePickerActionBar"));
		if(kUseLine)
			%init(LineHook, LineMessagingUI_ChatInputBar = NSClassFromString(@"LineMessagingUI.ChatInputBar"));
		if(kUseSignal)
			%init(SignalHook, Signal_ConversationInputToolbar = NSClassFromString(@"Signal.ConversationInputToolbar"));
		if(kUseReddit)
			%init(RedditHook, Chat_RedditChat_ChatTextInputViewController = NSClassFromString(@"Chat_RedditChat.ChatTextInputViewController"), 
			Reddit_MessageComposeViewController = NSClassFromString(@"Reddit.MessageComposeViewController"));
		if(kUseSMS)
			%init(SMSHook);
		if(kUseMail)
			%init(MailHook);
		if(kUsePhone) {
			%init(PhoneHook);
			%init(ContactsHook);
		}
		if(kUseWhatsAppCall || kUseWhatsAppMsg || kUseWhatsAppVideoCall)
			%init(WhatsAppHook);
		if(kUseViber)
			%init(ViberHooks);
		if(kUseWeChat)
			%init(WeChatHooks);

		if((kUseKaTalkMsg || kUseKaTalkCall) && [NSProcessInfo.processInfo.processName isEqualToString:@"KakaoTalk"]) {
			// NSString *bundlePath = NSBundle.mainBundle.bundlePath;
			// NSString *frameworkPath = [NSString stringWithFormat:@"%@%@", bundlePath, @"/KakaoTalk"];
			// void *dlImage = dlopen([frameworkPath UTF8String], RTLD_NOW);

			// MSImageRef image = MSGetImageByName([frameworkPath UTF8String]);

			%init(KakaoTalkHooks, KakaoTalk_MVoipMessageCellView = NSClassFromString(@"KakaoTalk.MVoipMessageCellView"),
			KakaoTalk_ChatInputBarView = NSClassFromString(@"KakaoTalk.ChatInputBarView"),
			// KakaoTalk_MessageCell = NSClassFromString(@"KakaoTalk.MessageCell"),
			KakaoTalk_ProfileBottomButton = NSClassFromString(@"KakaoTalk.ProfileBottomButton"));
			// KakaoTalk_ClosureSleeve = NSClassFromString(@"KakaoTalk.ClosureSleeve"));
			// MSHookFunction(dlsym(dlImage, "$sSo6UIViewC6TalkUIE13addTapHandleryyyycF"),(void*)hook_addTapHandler,(void**)&orig_addTapHandler);
		}

		//MSHookFunction((void*)dispatch_async, (void*)new_dispatch_async, (void**)&old_dispatch_async);
	}
}
